# eZ Platform AlloyEditor Element Width

This bundle allows editing the width style for any HTML elements in eZ Platform Online Editor.

## Installation

1. Require via composer
    ```bash
    composer require contextualcode/ezplatform-alloyeditor-element-width
    ```

2. Clear browser caches and enjoy!

## Usage

1. After this bundle is installed, by default it should be possible to edit width for table cells. But if you are using [custom configurations for Online Editor toolbars](https://doc.ezplatform.com/en/latest/guide/extending/extending_online_editor/#buttons-configuration), or if you want to edit width for other elements than table cells, just add `element-width` button to required toolbars. Example:
    ```yaml
    alloy_editor:
        extra_buttons:
            table: [element-width]
            ol: [element-width]
            ul: [element-width]
            th: [element-width]
            td: [element-width]
    ```
2. Open Online Editor and there should be "Edit Width" button in the enabled toolbars:
    ![edit_source_modal](https://gitlab.com/contextualcode/ezplatform-alloyeditor-element-width/raw/master/doc/images/edit_elements_width.png)